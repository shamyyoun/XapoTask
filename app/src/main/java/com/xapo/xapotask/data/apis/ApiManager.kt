package com.xapo.xapotask.data.apis

interface ApiManager {
    fun <S> createApi(apiClass: Class<S>): S
}