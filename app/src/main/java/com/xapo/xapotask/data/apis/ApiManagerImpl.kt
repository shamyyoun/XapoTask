package com.xapo.xapotask.data.apis

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiManagerImpl : ApiManager {
    private lateinit var retrofit: Retrofit
    private val retrofitBuilder = Retrofit.Builder()
    private val httpOk = OkHttpClient.Builder()

    constructor() {
        retrofitBuilder.baseUrl(END_POINT)
        retrofitBuilder.addConverterFactory(GsonConverterFactory.create())

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        httpOk.addInterceptor(interceptor)
        retrofitBuilder.client(httpOk.build())
    }

    override fun <S> createApi(apiClass: Class<S>): S {
        retrofit = retrofitBuilder.build()
        return retrofit.create(apiClass)
    }

    companion object {
        private const val END_POINT = "https://api.github.com/"
        private var apiManagerImpl: ApiManagerImpl? = null

        fun isntance(): ApiManagerImpl {
            if (apiManagerImpl == null) {
                apiManagerImpl = ApiManagerImpl()
            }

            return apiManagerImpl!!
        }
    }
}