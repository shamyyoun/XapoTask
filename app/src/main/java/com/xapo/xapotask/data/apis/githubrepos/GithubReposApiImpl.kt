package com.xapo.xapotask.data.apis.githubrepos

import com.xapo.xapotask.data.apis.ApiManagerImpl
import com.xapo.xapotask.data.models.GithubRepo
import com.xapo.xapotask.data.models.GithubReposResponse
import retrofit2.Call

class GithubReposApiImpl {
    private val apiManagerImpl = ApiManagerImpl.isntance()

    fun getTrending(queries: Map<String, String>): Call<GithubReposResponse> {
        val githubReposApi = apiManagerImpl.createApi(GithubReposApi::class.java)
        return githubReposApi.getTrending(queries)
    }
}