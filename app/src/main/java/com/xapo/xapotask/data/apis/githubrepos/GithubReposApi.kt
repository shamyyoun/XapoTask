package com.xapo.xapotask.data.apis.githubrepos

import com.xapo.xapotask.data.models.GithubReposResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface GithubReposApi {
    @GET("search/repositories")
    fun getTrending(@QueryMap queries: Map<String, String>): Call<GithubReposResponse>
}