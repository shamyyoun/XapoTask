package com.xapo.xapotask.ui.repodetails

import com.xapo.xapotask.App
import com.xapo.xapotask.data.models.GithubRepo
import com.xapo.xapotask.utils.BrowserUtils
import no.babo.android.ui.base.BaseViewModel

class RepoDetailsViewModel(app: App, val githubRepo: GithubRepo) : BaseViewModel(app) {

    fun openRepoUrl() {
        BrowserUtils.openUrl(getContext(), githubRepo.url)
    }
}