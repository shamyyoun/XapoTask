package com.xapo.xapotask.ui.trendingrepos

import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.Toast
import com.xapo.xapotask.R
import com.xapo.xapotask.data.models.GithubRepo
import com.xapo.xapotask.ui.repodetails.RepoDetailsActivity
import com.xapo.xapotask.utils.InjectionUtils
import com.xapo.xapotask.utils.KEY_GITHUB_REPO
import kotlinx.android.synthetic.main.activity_trending_repos.*
import no.babo.android.ui.base.BaseActivity

class TrendingReposActivity : BaseActivity() {
    private lateinit var viewModel: TrendingReposViewModel
    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trending_repos)

        // init views
        initViews()

        // customize the toolbar
        customizeToolbar()

        // get the view model
        getViewModel()

        // get the repos
        getData()
    }

    private fun initViews() {
        toolbar = findViewById(R.id.toolbar)
    }

    private fun customizeToolbar() {
        setSupportActionBar(toolbar)
        toolbar.title = getString(R.string.github_trending)
    }

    private fun getViewModel() {
        viewModel = InjectionUtils.getTrendingReposViewModel(this)
    }

    private fun getData() {
        // get
        viewModel.getRepos(viewModel.getRepoQueries())

        switchProgress(true)
        viewModel.liveData.observe(this, Observer {
            when (it) {
                null -> {
                    switchProgress(false)
                    Toast.makeText(this, R.string.failed_loading_repositories, Toast.LENGTH_LONG).show()
                }
                else -> {
                    switchProgress(false)
                    rvRepos.adapter = ReposAdapter(it, this::onItemClicked)
                }
            }
        })
    }

    private fun onItemClicked(view: View, githubRepo: GithubRepo) {
        openDetails(githubRepo)
    }

    private fun openDetails(githubRepo: GithubRepo) {
        startActivity(Intent(this, RepoDetailsActivity::class.java)
                .putExtra(KEY_GITHUB_REPO, githubRepo))
    }

    private fun switchProgress(show: Boolean) {
        when (show) {
            true -> viewProgress.visibility = View.VISIBLE
            false -> viewProgress.visibility = View.GONE
        }
    }
}