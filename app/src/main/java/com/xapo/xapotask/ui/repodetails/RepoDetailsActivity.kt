package com.xapo.xapotask.ui.repodetails

import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.View
import com.squareup.picasso.Picasso
import com.xapo.xapotask.R
import com.xapo.xapotask.data.models.GithubRepo
import com.xapo.xapotask.utils.InjectionUtils
import com.xapo.xapotask.utils.KEY_GITHUB_REPO
import kotlinx.android.synthetic.main.activity_repo_details.*
import no.babo.android.ui.base.BaseActivity

class RepoDetailsActivity : BaseActivity() {
    private lateinit var viewModel: RepoDetailsViewModel
    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repo_details)

        // init views
        initViews()

        // customize toolbar
        customizeToolbar()

        // get the view model
        getViewModel()

        // update the ui
        updateUI()
    }

    private fun initViews() {
        toolbar = findViewById(R.id.toolbar)
    }

    private fun customizeToolbar() {
        setSupportActionBar(toolbar)
        toolbar.setNavigationIcon(R.drawable.back_icon)
        toolbar.setNavigationOnClickListener { onBackPressed() }
    }

    private fun getViewModel() {
        viewModel = InjectionUtils.getRepoDetailsViewModel(this, getRepoFromIntent())
    }

    private fun getRepoFromIntent(): GithubRepo {
        return intent.getSerializableExtra(KEY_GITHUB_REPO) as GithubRepo
    }

    private fun updateUI() {
        with(viewModel.githubRepo) {
            toolbar.title = name
            tvDesc.text = description
            tvLang.text = language
            tvStarsCount.text = getString(R.string.n_stars, starsCount)
            tvForksCount.text = getString(R.string.n_forks, forksCount)

            Picasso.get()
                    .load(owner.avatarUrl)
                    .error(R.mipmap.ic_launcher)
                    .placeholder(R.mipmap.ic_launcher)
                    .into(ivAvatar)
        }
    }

    fun openRepoUrl(view: View) {
        viewModel.openRepoUrl()
    }
}