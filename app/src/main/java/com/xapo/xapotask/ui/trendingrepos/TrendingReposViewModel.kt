package com.xapo.xapotask.ui.trendingrepos

import android.arch.lifecycle.MutableLiveData
import com.xapo.xapotask.App
import com.xapo.xapotask.data.models.GithubRepo
import com.xapo.xapotask.data.repositories.GithubReposRepository
import no.babo.android.ui.base.BaseViewModel
import java.text.SimpleDateFormat
import java.util.*

class TrendingReposViewModel(app: App, private val githubReposRepository: GithubReposRepository) : BaseViewModel(app) {
    val liveData = MutableLiveData<List<GithubRepo>>()
    private val queries: HashMap<String, String> = HashMap()

    fun getRepos(queries: Map<String, String>) {
        githubReposRepository.getRepos(queries).observeForever {
            liveData.value = it
        }
    }

    fun getRepoQueries(): Map<String, String> {
        queries["sort"] = "stars"
        queries["order"] = "desc"
        queries["q"] = "language:kotlin+language:java pushed:>=${getCurrentDate()}"

        return queries
    }

    private fun getCurrentDate(): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        return sdf.format(Date())
    }
}