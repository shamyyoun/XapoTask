package no.babo.android.ui.base

import android.arch.lifecycle.AndroidViewModel
import android.content.Context
import com.xapo.xapotask.App

open class BaseViewModel(app: App) : AndroidViewModel(app) {

    protected fun getContext(): Context {
        return getApplication<App>().applicationContext
    }
}