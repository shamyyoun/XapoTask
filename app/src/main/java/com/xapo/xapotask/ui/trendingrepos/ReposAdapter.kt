package com.xapo.xapotask.ui.trendingrepos

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.xapo.xapotask.R
import com.xapo.xapotask.data.models.GithubRepo
import kotlinx.android.synthetic.main.item_repo.view.*

class ReposAdapter(var data: List<GithubRepo>, val onItemClickListener: (View, GithubRepo) -> Unit)
    : RecyclerView.Adapter<MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return MyViewHolder(layoutInflater.inflate(R.layout.item_repo, parent, false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        data[position].let {
            holder.itemView.tvName.text = it.fullName
            holder.itemView.tvDesc.text = it.description
            holder.itemView.tvLanguage.text = it.language
            holder.itemView.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    onItemClickListener(holder.itemView, it)
                }
            })
        }
    }
}

class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)