package com.xapo.xapotask.utils

import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import com.xapo.xapotask.App
import com.xapo.xapotask.data.apis.githubrepos.GithubReposApiImpl
import com.xapo.xapotask.data.models.GithubRepo
import com.xapo.xapotask.data.repositories.GithubReposRepository
import com.xapo.xapotask.ui.ViewModelCustomProvider
import com.xapo.xapotask.ui.repodetails.RepoDetailsViewModel
import com.xapo.xapotask.ui.trendingrepos.TrendingReposViewModel

object InjectionUtils {
    fun getTrendingReposViewModel(activity: AppCompatActivity): TrendingReposViewModel {
        val githubReposApiImpl = GithubReposApiImpl()
        val githubReposRepository = GithubReposRepository(githubReposApiImpl)

        return ViewModelProviders.of(
                activity,
                ViewModelCustomProvider(
                        TrendingReposViewModel(
                                activity.application as App,
                                githubReposRepository)))
                .get(TrendingReposViewModel::class.java)
    }

    fun getRepoDetailsViewModel(activity: AppCompatActivity, githubRepo: GithubRepo): RepoDetailsViewModel {
        return ViewModelProviders.of(
                activity,
                ViewModelCustomProvider(
                        RepoDetailsViewModel(
                                activity.application as App,
                                githubRepo)))
                .get(RepoDetailsViewModel::class.java)
    }
}