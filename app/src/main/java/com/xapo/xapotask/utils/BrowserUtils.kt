package com.xapo.xapotask.utils

import android.content.Context
import android.content.Intent
import android.net.Uri

object BrowserUtils {

    fun openUrl(context: Context, url: String) {
        val url = Uri.parse(url)
        val intent = Intent(Intent.ACTION_VIEW, url)
        context.startActivity(intent)
    }
}